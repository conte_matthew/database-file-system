
@echo Starting MYSQL process...
@ mysqld

@set /p mysql_pwd="Enter MYSQL root password to set up DFS database: 

@echo Creating DFS database on MYSQL server...
@ mysql -u root -p%mysql_pwd% -e "CREATE DATABASE dfs";
@echo Database created

@echo Importing database structure...
@ mysql -u root -p%mysql_pwd% dfs < dfs.sql
@echo Imported database structure

@echo Adding dfs user...
@ mysql -u root -p%mysql_pwd% -e "CREATE USER 'dfs'@'localhost' IDENTIFIED BY 'password'";
@ mysql -u root -p%mysql_pwd% -e "GRANT ALL ON dfs.* TO 'dfs'@'localhost'";
@echo Added dfs user

@echo Successfully set up the DFS database!

@PAUSE
