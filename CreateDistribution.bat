@echo off

mkdir dfs_dist
mkdir dfs_dist\bin
mkdir dfs_dist\bin\lib

copy dist\DatabaseFileSystem.jar dfs_dist\bin\DatabaseFileSystem.jar
copy dist\DatabaseFileSystem.jar dfs_dist\bin\lib\DatabaseFileSystem.jar
copy dist\lib\mysql-connector-java-5.1.23-bin.jar dfs_dist\bin\lib\mysql-connector-java-5.1.23-bin.jar

copy ..\DatabaseFileSystem_Updater\dist\DatabaseFileSystem_Updater.jar dfs_dist\bin\DatabaseFileSystem_Updater.jar

copy README dfs_dist\README

copy RunDFS.bat dfs_dist

xcopy install dfs_dist\install /E /I
xcopy properties dfs_dist\properties /E /I

PAUSE