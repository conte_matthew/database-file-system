package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.DirectoryObject.DirectoryHelper;
import databasefilesystem.filesystem.FileObject.FileHelper;
import databasefilesystem.filesystem.FilesystemObject;
import databasefilesystem.utility.Logger;
import java.util.ArrayList;

/**
 *
 * @author Matthew
 */
public class ShowAllCommand implements Command {
    private String cmdStr;
    private String objType;
    private DFSResultSet resultSet;
    
    public ShowAllCommand(String cmdStr) {
        this.cmdStr = cmdStr;
        resultSet = new DFSResultSet();
    }
    
    @Override
    public boolean execute() {
        Logger logger = new Logger("ShowAllCommand::execute");
        
        if (validateCmd()) {
            if (objType.equals("files"))
                resultSet = FileHelper.all();
            else if (objType.equals("directories"))
                resultSet = DirectoryHelper.all();
            else {
                resultSet = FileHelper.all();
                DFSResultSet dirResults = DirectoryHelper.all();
                //resultSet.getResults().addAll(dirResults.getResults());
            }
            
            return true;
        }
        else {
            logger.log(Logger.ERROR, cmdStr + " is not a valid command");
            return false;
        }
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return resultSet;
    }
    
    @Override
    public boolean hasResults() { return true; }
    
    @Override
    public boolean updatesWorkingSet() { return true; }
    
    /**
     * Validates that the command is valid. A valid "show_all" command either has no 
     *  arguments (meaning show all filesystem objects) or has a single argument specifying
     *  the type of objects to show ("files" or "directories")
     * @return 
     */
    private boolean validateCmd() {
        Logger logger = new Logger("ShowAllCommand::validateCmd");
        
        String[] tokens = cmdStr.split(" ");     
        if (tokens.length == 1) {
            objType = "all";
            return true;
        }       
        
        String searchType = tokens[1];
        if (!(searchType.equals("files") || searchType.equals("directories"))) {
            logger.log(Logger.ERROR, "Invalid object type: " + searchType);
            return false;
        }
                
        objType = searchType;
        
        return true;
    }
    
}
