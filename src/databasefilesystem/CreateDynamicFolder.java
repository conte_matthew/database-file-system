package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.DynamicFolder;
import java.util.Scanner;

public class CreateDynamicFolder implements Command {
    DFSResultSet resultSet;
    
    public CreateDynamicFolder(DFSResultSet resultSet) {
        this.resultSet = resultSet;
    }
    
    public DFSResultSet getResultSet() {
        return resultSet;
    }
    
    /**
     * Uses the passed-in DFSResultSet to create MySQL views in the database from
     *  the saved file and directory queries. Prompts the user for a name and
     *  description for this new folder and creates a database entry for the folder
     * @return 
     */
    @Override
    public boolean execute() {
        String folderName = "";
        String description = "";
        
        Scanner input = new Scanner(System.in);
        while (folderName.equals("")) {
            System.out.print("Choose a name for this folder: ");
            folderName = input.nextLine();
        }
        
        System.out.print("Add a description for this folder (optional): ");
        description = input.nextLine();
        
        String fileQuery = resultSet.getFileQuery();
        String dirQuery = resultSet.getDirectoryQuery();     
        
        DynamicFolder newFolder = new DynamicFolder(folderName, description, fileQuery, dirQuery);
        newFolder.save();
        
        return true;
    }
    
    @Override
    public boolean hasResults() { return false; }
   
    @Override
    public boolean updatesWorkingSet() { return false; }
    
    

}