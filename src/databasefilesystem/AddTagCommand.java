package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.DirectoryEntryObject;
import databasefilesystem.filesystem.DirectoryObject;
import databasefilesystem.filesystem.FileObject;
import databasefilesystem.filesystem.FilesystemObject;
import databasefilesystem.utility.Logger;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Matthew
 */
public class AddTagCommand implements Command {
    private static final int TAG_DIRECTORY_ONLY = 1;
    private static final int TAG_FILES          = 2;
    private static final int TAG_SUBDIRECTORIES = 3;
    private static final String DIR_TAGGING_OPTIONS = 
        "  1. Apply tag only to this directory\n" +
        "  2. Apply tag also to files within this directory\n" +
        "  3. Apply tag recursively to all files and subdirectories";
    
    private FilesystemObject obj = null;
    private String tagToAdd;
    
    public AddTagCommand(FilesystemObject obj, String tagToAdd) {
        this.obj = obj;
        this.tagToAdd = tagToAdd;
    }
    
    @Override
    public boolean execute() {
        Logger logger = new Logger("AddTagCommand::execute");
        
        if (obj == null) {
            logger.log(Logger.ERROR, "Filesystem object does not exist. Could not add tag.");
            return false;
        }
        
        // If this is a Directory, ask the user if they want to apply the tag to
        //  files in the directory, subfolders recursively, or just to the directory entry
        DirectoryObject dir;
        if (DirectoryObject.class.isAssignableFrom(obj.getClass())) {
            if (obj instanceof DirectoryObject)
                dir = (DirectoryObject)obj;
            else
                dir = (DirectoryEntryObject)obj;
                
            Scanner input = new Scanner(System.in);
            System.out.print("This object is a directory. Do you want to: \n" + DIR_TAGGING_OPTIONS + "\n");
            String cmdStr = input.nextLine();
            int choice = Integer.parseInt(cmdStr);
            while (choice != TAG_DIRECTORY_ONLY &&
                   choice != TAG_FILES          &&
                   choice != TAG_SUBDIRECTORIES) {
                input = new Scanner(System.in);
                System.out.print("Invalid choice. Do you want to: \n" + DIR_TAGGING_OPTIONS + "\n");
                cmdStr = input.nextLine();
                choice = Integer.parseInt(cmdStr);
            }
            
            dir.addTag(tagToAdd);
            switch (choice) {
                case TAG_DIRECTORY_ONLY:
                    break;
                case TAG_FILES:
                    applyTagToFiles(tagToAdd);
                    break;
                case TAG_SUBDIRECTORIES:
                    applyTagToFiles(tagToAdd);
                    
                    dir.addTagRecursive(tagToAdd);
                    break;
            }
        }
        // If object is a file, just add the tag
        else {       
            obj.addTag(tagToAdd);
        }
        
        return true;
    }
    
    private void applyTagToFiles(String tagToAdd) {
        ArrayList<FileObject> files = ((DirectoryObject)obj).getFiles();
        for (FileObject file : files) {
            file.addTag(tagToAdd);
        }
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return new DFSResultSet();
    }
    
    @Override
    public boolean hasResults() { return false; }
    
    @Override
    public boolean updatesWorkingSet() { return false; }
}
