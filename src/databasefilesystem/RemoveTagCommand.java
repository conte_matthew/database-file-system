package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.FilesystemObject;
import databasefilesystem.utility.Logger;

/**
 *
 * @author Matthew
 */
public class RemoveTagCommand implements Command {
    private FilesystemObject obj = null;
    private String tagToRemove;
    
    public RemoveTagCommand(FilesystemObject obj, String tagToRemove) {
        this.obj = obj;
        this.tagToRemove = tagToRemove;
    }
    
    @Override
    public boolean execute() {
        Logger logger = new Logger("RemoveTagCommand::execute");
        
        if (obj == null) {
            logger.log(Logger.ERROR, "Filesystem object does not exist. Could not remove tag.");
            return false;
        }
        
        obj.removeTag(tagToRemove);
        
        return true;
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return new DFSResultSet();
    }
    
    @Override
    public boolean hasResults() { return false; }
    
    @Override
    public boolean updatesWorkingSet() { return false; }
}
