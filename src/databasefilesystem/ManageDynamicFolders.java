
package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.DynamicFolder;
import databasefilesystem.filesystem.DynamicFolder.DynamicFolderHelper;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Matthew
 */
public class ManageDynamicFolders implements Command {
    DFSResultSet resultSet;
    
    public ManageDynamicFolders() {
        resultSet = new DFSResultSet();
    }
    
    
    /**
     * Display list of saved dynamic folders. Prompt user to select one whose
     *  contents to display or to delete.
     */
    @Override
    public boolean execute() {
        ArrayList<DynamicFolder> dynamicFolders = DynamicFolderHelper.all();   
        if (dynamicFolders.isEmpty()) {
            System.out.println("No dynamic folders saved");
            return true;
        }

        Scanner input = new Scanner(System.in);
        int folderNum = 0;
        while (true) {
            int i = 1;
            System.out.println("\nAvailable Dynamic Folders:");
            for (DynamicFolder folder : dynamicFolders) {
                System.out.println("\t" + i + ". " + folder.getName());
                i++;
            }

            System.out.print("\nEnter 'display' or 'delete' followed by the folder number (Press 'Enter' to return to main menu): ");
            String line = input.nextLine();
            if (line.equals(""))
                break;
            
            String[] cmdStr = line.split(" ");
            String cmd = cmdStr[0];
            folderNum = Integer.parseInt(cmdStr[1])-1;

            switch (cmd) {
                case "display":
                    System.out.println(dynamicFolders.get(folderNum).expand());
                    break;
                case "delete":
                    dynamicFolders.get(folderNum).delete();
                    dynamicFolders.remove(folderNum);
                    break;              
            }
            
        }
                        
        return true;
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return resultSet;
    }
    
    @Override
    public boolean hasResults() { return true; }
    
    @Override
    public boolean updatesWorkingSet() { return true; }
    
}
