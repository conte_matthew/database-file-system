
package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.DirectoryEntryObject;
import databasefilesystem.filesystem.DirectoryObject;
import java.io.File;

/**
 * This command takes a path to a directory (or prompts the user for one if none
 *  is provided) and scans the file hierarchy beneath that path, inserting the
 *  corresponding entries into the database.
 * @author Matthew
 */
public class ScanFileHierarchyCommand implements Command {
    private File dir;
    
    public ScanFileHierarchyCommand(String dirPath) {
        dir = new File(dirPath);
    }
    
    @Override
    public boolean execute() {
        if (dir.exists() && dir.isDirectory()) {
            DirectoryEntryObject dirEntryObj = DirectoryObject.DirectoryHelper.getDirectoryByFullPath(dir.getPath());
            boolean saved;
            
            // Directory already exists in database. Update its contents
            if (dirEntryObj != null) {
                dirEntryObj.update();
                saved = dirEntryObj.save();
                if (saved)
                    System.out.println(String.format("Successfully updated directory %s", dir.getPath()));
                else
                    System.out.println(String.format("Failed to update directory %s", dir.getPath()));
            }
            else {
                DirectoryObject dirObj = new DirectoryObject(dir);
                dirObj.expand();          
                saved = dirObj.save();
                if (saved)
                    System.out.println(String.format("Successfully scanned directory %s into database", dir.getPath()));
                else
                    System.out.println(String.format("Failed to scan directory %s into database", dir.getPath()));
            }
            return saved;          
        }
        else {
            System.out.println("Path does not exist or is not a directory");
            return false;
        }      
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return new DFSResultSet();
    }
    
    @Override
    public boolean hasResults() { return false; }
    
    @Override
    public boolean updatesWorkingSet() { return false; }
   
}
