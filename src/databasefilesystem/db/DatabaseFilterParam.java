
package databasefilesystem.db;

/**
 *
 * @author Matthew
 */
public class DatabaseFilterParam {
    public String fieldName;
    public String comparator;
    public String      value;
    
    public DatabaseFilterParam(String fieldName, String comparator, String value) {
        this.fieldName = fieldName;
        this.comparator = comparator;
        this.value = value;
    }
}
