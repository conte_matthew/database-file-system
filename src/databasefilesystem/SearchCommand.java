package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.db.DatabaseFilterParam;
import databasefilesystem.filesystem.DirectoryObject.DirectoryHelper;
import databasefilesystem.filesystem.FileObject.FileHelper;
import databasefilesystem.utility.Utility;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 *
 * @author Matthew
 */
public class SearchCommand implements Command {
    String searchStr;
    String searchType;
    ArrayList<DatabaseFilterParam> fileSearchParamList;
    ArrayList<DatabaseFilterParam> directorySearchParamList;
    DFSResultSet resultSet;
    
    public SearchCommand(String searchStr) {
        this.searchStr  = searchStr;  
        this.searchType = "";   
        
        fileSearchParamList = new ArrayList();
        directorySearchParamList = new ArrayList();
        resultSet = new DFSResultSet();
    }
    
    @Override
    public boolean execute () { 
        if (parseSearchString()) {
            if (searchType.equals("files"))
                resultSet = FileHelper.filter(fileSearchParamList);
            else if (searchType.equals("directories"))
                resultSet = DirectoryHelper.filter(directorySearchParamList);
            else {
                resultSet = FileHelper.filter(fileSearchParamList);
                DFSResultSet dirResults = DirectoryHelper.filter(directorySearchParamList);
                resultSet.getResults().addAll((ArrayList)dirResults.getResults());
                resultSet.setDirectoryQuery(dirResults.getDirectoryQuery());
            }
            
            return true;
        }
        
        return true; 
    }   
    
    @Override
    public boolean hasResults() { return true; }
    
    @Override
    public boolean updatesWorkingSet() { return true; }
    
    private boolean parseSearchString() {       
        StringTokenizer searchCmd = new StringTokenizer(this.searchStr);
        String tok;
        
        int numTokens = searchCmd.countTokens();
        if (numTokens == 0) {
            System.err.println("Search received blank command");
            return false;
        }
        tok = searchCmd.nextToken(); // Advance past the 'search' token
        
        if (numTokens == 1) {
            System.err.println("\"search\" command did not specify type of search ('files', 'directories', or 'all')");
            return false;
        }
        
        if (numTokens == 2) {
            System.err.println("\"search\" command not provided any parameters");
            return false;
        }
        
        tok = searchCmd.nextToken(); 
        if (tok.equals("files") || tok.equals("directories") || tok.equals("all"))
            this.searchType = tok;
        else {
            System.err.println("Invalid search type '" + searchType + "'. Must be 'files', 'directories', or 'all'");
            return false;
        }
        
        return parseSearchParams(searchCmd);
    }
    
    /**
     * Extracts [parameter, comparator, value] triples from a ';'-separated list
     *  in the search command. The params Tokenizer must already be advanced to the
     *  start of the parameters list so that the next call to getNextToken will 
     *  return the first parameter name
     * @param params
     * @return True if the parameter list is successfully parsed. False, otherwise
     */
    private boolean parseSearchParams(StringTokenizer params) {
        String param        = null;
        String comparator   = null;
        String value        = null;
        String tok;
        while (params.hasMoreTokens()) {                        
            tok = params.nextToken(); 

            // Comparator
            if (Utility.isComparator(tok)) {
                if (param == null) {                    
                    System.err.println(String.format("Badly-formed query. Cannot have '%s' without field name first", tok));
                    return false;
                }
                if (comparator != null) {
                    System.err.println(String.format("Cannot have two comparators in a row: '%s' and '%s", comparator, tok));
                    return false;
                }
                
                comparator = tok;
            }
            // Value
            else if (tok.endsWith(";")) {
                if (comparator == null) {
                    System.err.println("Badly-formed query. Cannot give value before comparator");
                    return false;
                }
                
                value = tok.substring(0, tok.length()-1);
                addToSearchParamLists(new DatabaseFilterParam(param, comparator, value));

                param        = null;
                comparator   = null;
                value        = null;               
            } 
            // Parameter name
            else {
                if (param != null && comparator == null) {
                    System.err.println(String.format("Cannot give 2 parameter fields in a row: %s and %s", param, tok));
                    return false;
                }    
                    
                // Translate human-readable parameter name into database table name
                if (!isParameterValid(tok)) {
                    System.err.println("Field name " + tok + " invalid.");
                    return false;
                }
                else
                    param = tok;
            }
        }
        
        return true;
    }
    
    /**
     * Returns true if paramName is valid for this query (it is an existing param name for
     *  the type of query being done or is valid for both files and directories if search
     *  type is 'all'. Returns false, otherwise
     * @param paramName
     * @return 
     */
    private boolean isParameterValid(String paramName) {
        String fileDbParam      = FileHelper.dbNameLookup(paramName);
        String directoryDbParam = DirectoryHelper.dbNameLookup(paramName);
        
        if (searchType.equals("files") && fileDbParam.equals("")) {
            System.err.println(String.format("Field %s is not a valid attribute of files", paramName));
            return false;
        }
        else if (searchType.equals("directories") && directoryDbParam.equals("")) {
            System.err.println(String.format("Field %s is not a valid attribute of directories", paramName));
            return false;
        }
        else if (searchType.equals("all") && (fileDbParam.equals("") || directoryDbParam.equals(""))) {
            System.err.println(String.format("Field %s does not apply to both files and directories", paramName));
            return false;
        }
        
        return true;
    }
    
    /**
     * Given a DatabaseFilterParam containing a parameter name, comparator, and
     *  value, looks up the corresponding database parameter name depending on which
     *  type of search is being done. Updates the parameter name and adds the DatabaseFilterParam
     *  to the correct parameter list(s).
     * @param param 
     */
    private void addToSearchParamLists(DatabaseFilterParam param) {
        String fileDbParam      = FileHelper.dbNameLookup(param.fieldName);
        String directoryDbParam = DirectoryHelper.dbNameLookup(param.fieldName);
                
        if (searchType.equals("files")) {
            fileSearchParamList.add(new DatabaseFilterParam(fileDbParam, param.comparator, param.value));
        }
        else if (searchType.equals("directories")) {
            directorySearchParamList.add(new DatabaseFilterParam(directoryDbParam, param.comparator, param.value));
        }
        else {
            fileSearchParamList.add(new DatabaseFilterParam(fileDbParam, param.comparator, param.value));
            directorySearchParamList.add(new DatabaseFilterParam(directoryDbParam, param.comparator, param.value));
        }
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return resultSet;
    }
    
}
