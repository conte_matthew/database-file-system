
package databasefilesystem.filesystem;

import databasefilesystem.db.DatabaseConnection;
import databasefilesystem.utility.Logger;
import java.io.IOException;
import java.io.InputStream;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Matthew
 */
public class DirectoryRoot {
    
    public static class DirectoryRootHelper {
        
        public static ArrayList<DirectoryRoot> all() {
            try {
                PreparedStatement query = DatabaseConnection.Instance().createQuery("SELECT * FROM directory_roots");           
                ResultSet results = query.executeQuery();
                return createObjectsFromResultSet(results);
            }
            catch (SQLException e) {
                System.err.println("DirectoryRootHelper::all failed to query database");
                return new ArrayList();
            }
        }
        
        public static ArrayList<DirectoryRoot> createObjectsFromResultSet(ResultSet results) {
            ArrayList<DirectoryRoot> resultSet = new ArrayList();
            try {
                DirectoryRoot dirRoot;
                while (results.next()){             
                    dirRoot = 
                        new DirectoryRoot(results.getLong("id"),
                                            results.getLong("d_id"),
                                            results.getString("path"),
                                            results.getInt("update_process_id"));                                                        
                    resultSet.add(dirRoot);
                }
            }
            catch (SQLException e) {
                Logger logger = new Logger("DirectoryHelper::createObjectsFromResultSet");
                logger.log(Logger.ERROR, "Failed to create directory objects from results set: " + e);
            }

            return resultSet;
        }
    }
    
    private long id;
    private long dirID;
    private String path;
    private int updateProcessID;
    
    private boolean dbEntryExists;
    
    /**
     * External constructor for creating a new DirectoryRoot database entry without
     *  a PID for the updater process. (Spawning an updater process is optional
     * @param dirID
     * @param path 
     */
    public DirectoryRoot(long dirID, String path) {
        this.dirID = dirID;
        this.path  = path;
        this.updateProcessID = 0;
        
        dbEntryExists = false;
    }
    
    /**
     * External constructor for creating a new DirectoryRoot database entry with
     *  a PID for the updater process
     * @param dirID
     * @param path
     * @param pid
     */
    public DirectoryRoot(long dirID, String path, int pid) {
        this.dirID = dirID;
        this.path  = path;
        this.updateProcessID = pid;
        
        dbEntryExists = false;
    }
    
    /**
     * Internal constructor for building a DirectoryRoot object from a database
     *  entry.
     * 
     * @param id
     * @param dirID
     * @param path
     * @param updateProcessID 
     */
    private DirectoryRoot(long id, long dirID, String path, int updateProcessID) {
        this.id                 = id;
        this.dirID              = dirID;
        this.path               = path;
        this.updateProcessID    = updateProcessID;
        
        dbEntryExists = true;
    }
    
    public boolean save() {
        Logger logger = new Logger("DirectoryRoot::save");
        try {
            PreparedStatement query;
            if (!dbEntryExists) { 
                query = DatabaseConnection.Instance().createQuery(
                        "INSERT INTO directory_roots (id, d_id, path, update_process_id) VALUES (?,?,?,?)");
                query.setLong(1, id);
                query.setLong(2, dirID);
                query.setString(3, path);
                query.setInt(4, updateProcessID);
                query.execute();
                
                dbEntryExists = true;
            }
            else {
                query = DatabaseConnection.Instance().createQuery(
                        "UPDATE directory_roots SET d_id=?, path=?, update_process_id=? WHERE id=?");               
                query.setLong(1, dirID);
                query.setString(2, path);
                query.setInt(3, updateProcessID);
                query.setLong(4, id);
                query.execute();
            }
        }
        catch (SQLException e) {
            logger.log(Logger.ERROR, String.format("Failed to save directory root entry %s: %s", path, e.toString()));
            return false; 
        }
        
        return true;
    }
    
    public boolean delete() {
        Logger logger = new Logger("DirectoryRoot::delete");
        
        try {
            // Delete the actual directory/file entries under this root
            System.out.println("Deleting root entry for directory with ID: " + this.dirID);
            DirectoryObject dirObj = new DirectoryObject(this.dirID);
            dirObj.delete();
            
            // Kill the update process
            try {
                Runtime.getRuntime().exec("taskkill /pid " + updateProcessID + " /f");
            }
            catch (IOException e) {
                logger.log(Logger.ERROR, 
                           String.format(
                                   "Failed to kill updater process for directory root %s: %s\nPID on record is %d", path, e.toString(), updateProcessID));
            }
            
            // Remove the entry from the directory root table
            PreparedStatement query = DatabaseConnection.Instance().createQuery("DELETE FROM directory_roots WHERE id=?");
            query.setLong(1, id);
            query.execute();            
        }
        catch (SQLException e) {           
            logger.log(Logger.ERROR, String.format("Failed to delete directory root %s: %s", path, e.toString()));
            return false;
        }
        
        return true;
    }
    
    /**
     * If this directory root does not currently have a process which periodically 
     *  checks for updates, create one.
     * @return 
     */
    public boolean startTracking() {
        Logger logger  = new Logger("DirectoryRoot::startTracking");
        if (updateProcessID == 0) {
            try {                      
                Process p = Runtime.getRuntime().exec("java -jar bin\\DatabaseFileSystem_Updater.jar " + this.getPath());

                // Retrieve PID to track the process in the database
                byte[] buffer = new byte[256];           
                InputStream reader = p.getInputStream();
                reader.read(buffer, 0, buffer.length);
                String processName = new String(buffer);

                updateProcessID = Integer.parseInt(processName.split("@")[0]);
            }
            catch (NumberFormatException e) {
                logger.log(Logger.ERROR, "Could not retrieve PID of update process: " + e);
                return false;
            }
            catch (IOException e) {
                logger.log(Logger.ERROR, "Failed to start update process");
                return false;
            }
        }
        
        return true;
    }
    
    /**
     * If this directory root currently has a process which periodically 
     *  checks for updates, kill the process.
     * @return 
     */
    public boolean stopTracking() {
        Logger logger  = new Logger("DirectoryRoot::stopTracking");
        if (updateProcessID != 0) {
            try {
                Runtime.getRuntime().exec("taskkill /pid " + updateProcessID + " /f");
            }
            catch (IOException e) {
                logger.log(Logger.ERROR, 
                           String.format(
                                   "Failed to kill updater process for directory root %s: %s\nPID on record is %d", path, e.toString(), updateProcessID));
                return false;
            }
        }
        
        updateProcessID = 0;
        return true;
    }
    
    public long getDirID()  { return dirID; }
    public String getPath() { return path; }
    public int getPID()     { return updateProcessID; }
    
    public void setUpdateProcessID(int pid) {
        this.updateProcessID = pid;
    }
    
    @Override
    public String toString() {
        String output = "";
        output = String.format("%-50s  [Tracking: %3s]", this.path, (updateProcessID == 0) ? "off" : "on");
        return output;
    }
    
}
