package databasefilesystem.filesystem;

import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 * Wrapper around a result set of FilesystemObjects which also stores the SQL 
 *  query used to retrieve that result set.
 * @author Matthew
 */
public class DFSResultSet {
    private String fileQuery;
    private String dirQuery;
    private ArrayList<? extends FilesystemObject> results;
    
    /**
     * For creating a blank result set on a failed query
     */
    public DFSResultSet() {
        this.fileQuery = "";
        this.dirQuery = "";
        this.results = new ArrayList();
    }
    
    public DFSResultSet(PreparedStatement fileStmt, PreparedStatement dirStmt, ArrayList<? extends FilesystemObject> results) {
        // PreparedStatement.toString doesn't simply give the SQL query but prints 
        //  something in the form: com.mysql.jdbc.JDBC4PreparedStatement@b81eda8: SELECT * FROM directories ...'
        // Need to extract just the query by splitting it at the first ' '
        String queryToString;
        if (fileStmt != null) {
            queryToString = fileStmt.toString();
            this.fileQuery = queryToString.substring(queryToString.indexOf(" ")+1);
        }
        else
            this.fileQuery = "";
        
        if (dirStmt != null) {
            queryToString = dirStmt.toString();
            this.dirQuery = queryToString.substring(queryToString.indexOf(" ")+1);
        }
        else
            this.dirQuery = "";
        
        this.results = results;
    }
    
    /**
     * Saves the query used to retrieve file records for this result set
     * @param fileStmt 
     */
    public void setFileQuery(PreparedStatement fileStmt) {
        String queryToString = fileStmt.toString();
        this.fileQuery = queryToString.substring(queryToString.indexOf(" ")+1);
    }
    
    public void setFileQuery(String fileQuery) {
        this.fileQuery = fileQuery;
    }
    
    /**
     * Returns the SQL query used to retrieve the file objects in the result set
     * @return 
     */
    public String getFileQuery() {
        return fileQuery;
    }
    
    /**
     * Saves the query used to retrieve directory records for this result set
     * @param dirStmt 
     */
    public void setDirectoryQuery(PreparedStatement dirStmt) {
        String queryToString = dirStmt.toString();
        this.dirQuery = queryToString.substring(queryToString.indexOf(" ")+1);
    }
    
    public void setDirectoryQuery(String dirQuery) {
        this.dirQuery = dirQuery;
    }
    
    /**
     * Returns the SQL query used to retrieve the directory objects in the result set
     * @return 
     */
    public String getDirectoryQuery() {
        return dirQuery;
    }
    
    /**
     * Returns the result set
     * @return 
     */
    public ArrayList<? extends FilesystemObject> getResults() {
        return results;
    }
}
