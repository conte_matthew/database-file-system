package databasefilesystem.filesystem;

import databasefilesystem.db.DatabaseConnection;
import databasefilesystem.db.DatabaseFilterParam;
import databasefilesystem.utility.Logger;
import databasefilesystem.utility.Utility;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Matthew
 */
public class FileObject extends FilesystemObject {
    
    public static class FileHelper {
        private static final String DB_NAME         = "files";
        private static final String ID_FIELD_NAME   = "f_id";
        private static final String TAG_TABLE_NAME  = "file_tags";
        
        private static final String fileSelectStatement = 
                "SELECT files.f_id, files.d_id, CONCAT(directories.parent_dir, \"\\\\\", directories.name) AS parent_dir, " +
                "files.name, files.ext, files.size, files.mod_time, files.readonly " +
                "FROM files JOIN directories " +
                "WHERE files.d_id = directories.d_id ";
        
        /**
         * This function translates human-readable field names into the database
         *  table names.
         * @param name
         * @return 
         */
        public static String dbNameLookup(String fieldName) {
            switch (fieldName) {
                case "dir":
                case "directory":
                case "parent":
                    return "CONCAT(directories.parent_dir, \"\\\\\", directories.name)";
                case "name":
                    return "files.name";
                case "ext":
                case "extension":
                    return "files.ext";
                case "size":
                    return "files.size";
                case "readonly":
                    return "files.readonly";
                default:
                    return "";
            }
        }
        
        public static DFSResultSet all() {
            try {
                PreparedStatement query = DatabaseConnection.Instance().createQuery(fileSelectStatement);               
                return new DFSResultSet(query, null, createObjectsFromResultSet(query.executeQuery()));
            }
            catch (SQLException e) {
                System.err.println("FileHelper::all failed to query database");
                return new DFSResultSet();
            }
        } 

        public static DFSResultSet filter(ArrayList<DatabaseFilterParam> filterParams) {  
            try {
                String queryStr = fileSelectStatement;

                if (!filterParams.isEmpty()) 
                    queryStr += " AND " + Utility.buildWhereClause(filterParams);

                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                return new DFSResultSet(query, null, createObjectsFromResultSet(query.executeQuery()));
            }
            catch (SQLException e) {
                System.err.println("FileHelper::filter failed to query database: " + e);
                return new DFSResultSet();
            }
        }
        
        public static DFSResultSet getFilesByParentDirId(long parentDirId) {
            try {
                String queryStr = fileSelectStatement + "AND files.d_id=?";
                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                query.setLong(1, parentDirId);
                return new DFSResultSet(query, null, createObjectsFromResultSet(query.executeQuery()));
            }
            catch (SQLException e) {
                System.err.println("FileHelper::getFilesByParentDirId failed to query database: " + e);
                return new DFSResultSet();
            }
        }
        
        public static FileObject getFileByFullPath(String path) {
            FileObject fileEntry = null;
            try {
                String queryStr = fileSelectStatement + " AND CONCAT(CONCAT(CONCAT(directories.parent_dir, '\\\\', directories.name), \"\\\\\", files.name), '.', files.ext) = ?";
                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                query.setString(1, path);
                ArrayList<FileObject> results = createObjectsFromResultSet(query.executeQuery());
                if (results.size() > 0)
                    fileEntry = results.get(0);
            }
            catch (SQLException e) {
                System.err.println("FileHelper::getFileByFullPath failed to query database: " + e);
                return null;
            }
            
            return fileEntry;
        }

        public static ArrayList<FileObject> createObjectsFromResultSet(ResultSet results) {
            ArrayList<FileObject> resultSet = new ArrayList();
            try {
                FileObject file;
                while (results.next()){
                    file = 
                        new FileObject(results.getInt("f_id"),
                                       results.getInt("d_id"),
                                       results.getString("parent_dir"),
                                       results.getString("name"),
                                       results.getString("ext"),
                                       results.getInt("size"),
                                       results.getTimestamp("mod_time").getTime(),
                                       results.getBoolean("readonly"));
                    
                    // Retrieve tags for this file and create the tag list
                    PreparedStatement query = 
                            DatabaseConnection.Instance().createQuery("SELECT * FROM file_tags WHERE f_id=?");
                    query.setLong(1, file.id);
                    
                    ResultSet tags = query.executeQuery();
                    while (tags.next()) {
                        file.tags.add(new Tag(file.getID(), tags.getString("tag")));
                    }
                    
                    resultSet.add(file);
                }
            }
            catch (SQLException e) {
                Logger logger = new Logger("FileHelper::createObjectsFromResultSet");
                logger.log(Logger.ERROR, "Failed to create file objects from results set: " + e);
            }

            return resultSet;
        }
    }
    
    
    
    private String  extension;
    private long    size;           // Size of file, in bytes
    private boolean readonly;
    
    /**
     * Public constructor to create a FileObject given the parent directory and
     *  the file's stat information
     */
    public FileObject(File file) {      
        super(file.getName(), file.getParent(), file.lastModified());
        
        this.size = file.length();
        this.readonly = !file.canWrite();
        
         // Check if this directory already exists in the database
        FileObject fileObj = FileHelper.getFileByFullPath(file.getPath());
        if (fileObj != null) {
            id          = fileObj.getID();
            tags        = fileObj.getTags();
            
            dbEntryExists = true;           
        }
        else {
            if (this.name.contains(".")) {
                int extPos = this.name.lastIndexOf(".");
                this.extension = this.name.substring(extPos+1);
                this.name = this.name.substring(0, extPos);
            }
            else
                this.extension = "";
        }
        
        save();
    }
    
    /**
     * Internal constructor for building an object from database results
     * 
     * @param id
     * @param directory_id
     * @param name
     * @param extension
     * @param size (in bytes)
     * @param mod_time
     * @param readonly 
     */
    private FileObject(int id, int parentDirID, String path, String name, String extension, 
                       int size, long modTime, boolean readonly) {
        super(name, path, modTime);
        this.id = id;
        this.parentDirID = parentDirID;
        this.extension = extension;
        this.size = size;
        this.readonly = readonly;      
        
        dbEntryExists = true;
    }
    
    /**
     * Writes the object's data back to the database
     * @return true if the operation was successful. false, otherwise
     */
    @Override
    public boolean save() {         
        try {
            PreparedStatement query;
            if (!dbEntryExists) {
                query = DatabaseConnection.Instance().createQuery(
                        "INSERT INTO files (d_id, name, ext, size, mod_time, readonly) VALUES (?, ?, ?, ?, ?, ?)");
                if (query != null) {
                    query.setLong(1, getParentDirID());
                    query.setString(2, this.name);
                    query.setString(3, this.extension);
                    query.setLong(4, this.size);
                    query.setTimestamp(5, new Timestamp(this.modTime)); 
                    query.setInt(6, (this.readonly ? 1 : 0));
                    query.executeUpdate();

                    ArrayList<Long> keys = DatabaseConnection.Instance().getGeneratedKeys(query);
                    this.id = keys.get(0);

                    dbEntryExists = true;
                }
            }
            else {
                query = DatabaseConnection.Instance().createQuery(
                        "UPDATE files SET d_id=?, name=?, ext=?, size=?, mod_time=?, readonly=? WHERE f_id=?");
                if (query != null) {
                    query.setLong(1, parentDirID);
                    query.setString(2, this.name);
                    query.setString(3, this.extension);
                    query.setLong(4, this.size);
                    query.setTimestamp(5, new Timestamp(this.modTime));
                    query.setInt(6, (this.readonly ? 1 : 0)); 
                    query.setLong(7, this.id); 
                    query.executeUpdate();
                }
            }

            saveTags();
            
            return true;
        }
        catch (SQLException e) {
            Logger logger = new Logger("FileObject::save");
            logger.log(Logger.ERROR, String.format("Failed to save file %s: %s", getPath(), e.toString()));
            return false;
        }
    }
    
    /**
     * Deletes the object from the database
     * @return true if the operation was successful. false, otherwise
     */
    @Override
    public boolean delete() {
        try {
            PreparedStatement query = DatabaseConnection.Instance().createQuery("DELETE FROM files WHERE f_id=?");
            query.setLong(1, id);
            query.execute();
        }
        catch (SQLException e) {
            Logger logger = new Logger("FileObject::delete");
            logger.log(Logger.ERROR, String.format("Failed to delete file %s: %s", getPath(), e.toString()));
            return false;
        }
        
        return true;
    }
    
    /**
     * Reads information from the filesystem entry for this file and updates the
     *  information in the database record
     * @return 
     */
    @Override
    public boolean update() {
        File file = new File(getPath());
        this.size     = file.length();
        this.readonly = !file.canWrite(); 
        this.modTime  = file.lastModified();
        
        return save();
    }
    
    @Override
    public String   getPath()       { return Utility.concatFileExtension(Utility.concatFilePath(path, name), getExtension()); }
    public String   getExtension()  { return extension; }
    public long     getSize()       { return size; }       
    public boolean  isReadonly()    { return readonly; }
    
    @Override
    protected String getDbName() { return FileHelper.DB_NAME; }
    @Override
    protected String getIDFieldName() { return FileHelper.ID_FIELD_NAME; }   
    @Override
    protected String getTagTableName() { return FileHelper.TAG_TABLE_NAME; }
    
    /**
     * Returns the name of the file and its extension
     * @return 
     */
    @Override
    public String getName() {
        return name + "." + extension;
    } 
    
    @Override
    public String toString() {
        String fileStr = String.format("%-20s %-5s  %-10d %-50s",
                             new SimpleDateFormat("MM/dd/yyyy KK:mm a").format(getLastModTime()),
                                 "",
                                 getSize(),
                                 getName());        
        fileStr += generateTagString();
        
        return fileStr;
    }   
}
