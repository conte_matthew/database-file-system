
package databasefilesystem.filesystem;

/**
 * Simple structure to keep track of one FilesystemObject tag
 * @author Matthew
 */
public class Tag implements Comparable<Tag> {
    public long id;     // If -1, means the tag has not yet been saved to DB
    public String tag;  // The tag itself
        
    public Tag(String tag) {
        this.id  = -1;
        this.tag = tag;
    }
    
    public Tag(long tagID, String tag) {
        this.id   = tagID;
        this.tag  = tag;
    }
    
    public boolean saved() { 
        return (id != -1); 
    }
    
    @Override
    public int compareTo(Tag t) {
        return this.tag.compareTo(t.tag);
    }
    
    @Override
    public boolean equals(Object object)
    {
        boolean equal = false;
        if (object != null && object instanceof Tag)
            equal = this.tag.equals(((Tag) object).tag);
       
        return equal;
    }
    
    public String toString() { return tag; }
}
