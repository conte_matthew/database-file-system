
package databasefilesystem.filesystem;

import databasefilesystem.db.DatabaseConnection;
import databasefilesystem.utility.Logger;
import databasefilesystem.utility.Utility;
import java.io.File;
import java.sql.PreparedStatement;
import java.util.ArrayList;

/**
 *
 * @author Matthew
 */
public abstract class FilesystemObject {   
    protected long   id;
    protected long   parentDirID;
    protected String name;
    protected String path;
    protected long   modTime;
    protected ArrayList<Tag> tags;
    
    //protected static  String dbTableName = "";
    protected boolean dbEntryExists;
    protected boolean saved;
    
    protected FilesystemObject() {
        this.tags = new ArrayList();
        
        dbEntryExists = false;
        saved         = false;
    }
    
    /**
     * Constructor for creating a FilesystemObject from the database using an
     *  existing ID
     * @param id 
     */
    protected FilesystemObject(long id) {
        this.id = id;
        
        this.tags = new ArrayList();
        dbEntryExists = true;
        saved         = true;
    }
    
    /**
     * Constructor for creating a new FilesystemObject
     * @param name
     * @param path
     * @param modTime 
     */
    protected FilesystemObject(String name, String path, long modTime) {
        this.name        = name;
        this.path        = path;
        this.modTime     = modTime;
        this.parentDirID = 0;
        
        this.tags = new ArrayList();
        
        dbEntryExists = false;
        saved         = false;
    }  
    
    /**
     * Writes the object's data back to the database
     * @return true if the operation was successful. false, otherwise
     */
    public boolean save() { return false; }
      
    /**
     * Deletes the object from the database
     * @return true if the operation was successful. false, otherwise
     */
    public boolean delete() { return false; }
    
    /**
     * Overwrite the information in the database with the latest data from the
     *  actual object residing on the filesystem.
     * @return true if the operation was successful. false, otherwise
     */
    public boolean update() { return false; }
    
    /**
     * Insert any tags this object has which have not yet been saved. Tags cannot be 
     *  updated so any tags already in the DB are ignored
     * @return 
     */
    protected boolean saveTags() { 
        String queryStr = 
            "INSERT INTO " + getTagTableName() + " (" + getIDFieldName() + ", tag) VALUES (?, ?)";
        try {
            for (Tag tag : tags) {
                if (!tag.saved()) {
                    PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                    query.setLong(1, id);
                    query.setString(2, tag.tag);
                    query.executeUpdate();
                    
                    ArrayList<Long> keys = DatabaseConnection.Instance().getGeneratedKeys(query);
                    tag.id = keys.get(0);
                }
            }
        }
        catch (Exception e) {
            Logger logger = new Logger("FilesystemObject::saveTags");
            logger.log(Logger.ERROR, "Failed to save tags to database: " + e);
            
            return false;
        }
        
        return true;
    }
          
    /**
     * If tag is not already in the object's list of tags, add it to the list
     *  and set the saved flag to false to indicate the object must be written
     *  back to the database
     * @param tag 
     */   
    public void addTag(String tag) {
        if (tags.indexOf(new Tag(tag)) == -1) {
            tags.add(new Tag(tag));
            saved = false;
        }
        
        save();
    }
    
    /**
     * If tag exists in the object's list of tags, remove it from the list and
     *  the database
     * @param tag 
     */
    public boolean removeTag(String tag) {
        if (!tags.contains(new Tag(tag))) {
            System.err.println("Filesystem object does not contain tag '" + tag + "'");
            return false;
        }
        
        try {
            String queryStr = "DELETE FROM " + getTagTableName() + " WHERE " + getIDFieldName() + " = " + id + " AND tag = ?";
            PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
            query.setString(1, tag);
            query.executeUpdate();
        }
        catch (Exception e) {
            Logger logger = new Logger("FilesystemObject::removeTag");
            logger.log(Logger.ERROR, "Failed to remove tag '" + tag + "' from database: " + e);
            
            return false;
        }
        
        for (int i = 0; i < tags.size(); i++) {
            if (tags.get(i).tag.equals(tag))
                tags.remove(i);
        }
        
        return true;
    }
    
    /**
     * Creates a string containing all of the object's tags to be displayed as part
     *  of a file/directory listing
     * @return 
     */
    protected String generateTagString() {
        String tagStr = "";
        ArrayList<Tag> tags = getTags();
        if (tags.size() > 0) {
            tagStr += " <Tags: ";
            for (int i = 0; i < tags.size(); i++) {
                tagStr += tags.get(i);
                if (i+1 != tags.size())
                    tagStr += ", ";
            }
            
            tagStr += ">";
        }
        
        return tagStr;
    }

    
    public long getID()             { return id; }
    public long getParentDirID()    { return parentDirID; }
    public String getName()         { return name; }
    public String getPath()         { return Utility.concatFilePath(path, name); }
    public String getParentPath()   { return path; }
    public long getLastModTime()    { return modTime; }
    public ArrayList<Tag> getTags() { return tags; }
    
    public void setParentDirID(long parentDirID) { this.parentDirID = parentDirID; }  
    
    protected abstract String getDbName();
    protected abstract String getIDFieldName();
    protected abstract String getTagTableName();
}
