/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package databasefilesystem.filesystem;

import databasefilesystem.db.DatabaseConnection;
import databasefilesystem.db.DatabaseFilterParam;
import databasefilesystem.filesystem.FileObject.FileHelper;
import databasefilesystem.utility.Logger;
import databasefilesystem.utility.Utility;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;

/**
 *
 * @author Matthew
 */
public class DirectoryObject extends FilesystemObject {
    
    /**
     * Helper class for retrieving Database records from the database
     */
    public static class DirectoryHelper {
        private static final String DB_NAME         = "directories";
        private static final String ID_FIELD_NAME   = "d_id";
        private static final String TAG_TABLE_NAME  = "directory_tags";

        public static String dbNameLookup(String name) {
            switch (name) {
                case "id":
                    return "directories.id";
                case "dir":
                case "directory":
                case "parent":
                    return "directories.parent_dir";
                case "name":
                    return "directories.name";
                default:
                    return "";
            }
        }

        /**
         * Returns all records for the object from the database
         * @return An ArrayList containing an object for each record in the database
         */
        public static DFSResultSet all() { 
            try {
                PreparedStatement query = DatabaseConnection.Instance().createQuery("SELECT * FROM directories");               
                return new DFSResultSet(null, query, createObjectsFromResultSet(query.executeQuery()));
            }
            catch (SQLException e) {
                System.err.println("DirectoryHelper::all failed to query database");
                return new DFSResultSet();
            }
        }

        /**
         * Returns all records for the object from the database which match the criteria
         *  given in filterParams. Each filterParam is a field_name, value pair and is combined
         *  to form a WHERE clause in a database query. 
         */
        public static DFSResultSet filter(ArrayList<DatabaseFilterParam> filterParams) {   
            try {
                String queryStr = "SELECT * FROM directories" +
                                  " WHERE " + Utility.buildWhereClause(filterParams);
                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);

                Logger logger = new Logger("DirectoryHelper::filter");
                //logger.log(Logger.INFO, "Query: " + query);
                DatabaseConnection db = DatabaseConnection.Instance();
                return new DFSResultSet(null, query, createObjectsFromResultSet(query.executeQuery()));
            }
            catch (SQLException e) {
                System.err.println("DirectoryHelper::filter failed to query database");
                return new DFSResultSet();
            }
        }
        
        public static DFSResultSet getDirectoriesByParentDirId(long parentDirId) {
            try {
                String queryStr = "SELECT * FROM directories WHERE parent_dir_id=?";
                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                query.setLong(1, parentDirId);
                return new DFSResultSet(null, query, createObjectsFromResultSet(query.executeQuery()));
            }
            catch (SQLException e) {
                System.err.println("DirectoryHelper::getDirectoriesByParentDirId failed to query database: " + e);
                return new DFSResultSet();
            }
        }
        
        public static DirectoryEntryObject getDirectoryByFullPath(String path) {
            DirectoryEntryObject dirEntry = null;
            try {
                String queryStr = "SELECT * FROM directories WHERE CONCAT(parent_dir, \"\\\\\", name) = ?";
                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                query.setString(1, path);
                ArrayList<DirectoryEntryObject> results = createObjectsFromResultSet(query.executeQuery());
                if (results.size() > 0)
                    dirEntry = results.get(0);
            }
            catch (SQLException e) {
                System.err.println("DirectoryHelper::getDirectoryByFullPath failed to query database: " + e);
                return null;
            }
            
            return dirEntry;
        }

        public static ArrayList<DirectoryEntryObject> createObjectsFromResultSet(ResultSet results) {
            ArrayList<DirectoryEntryObject> resultSet = new ArrayList();
            try {
                DirectoryEntryObject dir;
                while (results.next()){             
                    dir = 
                        new DirectoryEntryObject(results.getInt("d_id"),
                                            results.getInt("parent_dir_id"),
                                            results.getString("parent_dir"),
                                            results.getString("name"),
                                            results.getTimestamp("mod_time").getTime());
                    
                    // Retrieve tags for this file and create the tag list
                    PreparedStatement query = 
                            DatabaseConnection.Instance().createQuery("SELECT * FROM directory_tags WHERE d_id=?");
                    query.setLong(1, dir.id);
                    
                    ResultSet tags = query.executeQuery();
                    while (tags.next()) {
                        dir.tags.add(new Tag(dir.getID(), tags.getString("tag")));
                    }
                    
                    resultSet.add(dir);
                }
            }
            catch (SQLException e) {
                Logger logger = new Logger("DirectoryHelper::createObjectsFromResultSet");
                logger.log(Logger.ERROR, "Failed to create directory objects from results set: " + e);
            }

            return resultSet;
        }
    }
    
    private ArrayList<FilesystemObject> dirEntries;
     
    public DirectoryObject() {}
    
    /**
     * Public constructor to create a DirectoryObject given the parent directory and
     *  the directory's stat information
     * @param dir: File object representing the directory to be created
     */
    public DirectoryObject(File dir) {
        super(dir.getName(), dir.getParent(), dir.lastModified());
        dirEntries = new ArrayList();
        
        // Check if this directory already exists in the database
        DirectoryEntryObject dirObj = DirectoryHelper.getDirectoryByFullPath(dir.getPath());
        if (dirObj != null) {
            id          = dirObj.getID();
            tags        = dirObj.getTags();
            
            dirEntries.addAll(DirectoryHelper.getDirectoriesByParentDirId(this.id).getResults());
            dirEntries.addAll(FileHelper.getFilesByParentDirId(this.id).getResults());
            
            dbEntryExists = true;  
        }
        else {       
            // Otherwise, create an entry for each file and directory in this directory. Do not
            //  expand 
            String[] dirEntryNames = dir.list();
            for (String dirEntryName : dirEntryNames)
                addNewFilesystemEntry(dirEntryName);
        }
    }
    
    /**
     * Internal constructor for building an object from an ID
     * 
     * @param id
     **/
    public DirectoryObject(long id) {
        super(id);
        
        dirEntries = new ArrayList();
        
        try {
            // Get directory entry info
            String queryStr = String.format("SELECT * FROM directories WHERE d_id=%d", this.id);
            DatabaseConnection db = DatabaseConnection.Instance();
            ResultSet results = db.query(queryStr);
            while (results.next()){   
                this.parentDirID    = results.getInt("parent_dir_id");
                this.path           = results.getString("parent_dir");
                this.name           = results.getString("name");
                this.modTime        = results.getTimestamp("mod_time").getTime();
            }
            
            // Get directory entries 
            dirEntries.addAll(DirectoryHelper.getDirectoriesByParentDirId(this.id).getResults());
            
            // Get file entries
            dirEntries.addAll(FileHelper.getFilesByParentDirId(this.id).getResults());
        } 
        catch (SQLException e) {
            Logger logger = new Logger("DirectoryObject::constructor");
            logger.log(Logger.ERROR, "Failed to create directory objects from results set: " + e);
        }
    }
    
    /**
     * Internal constructor for building an object from database results
     * 
     * @param id
     * @param parentDirID
     * @param path
     * @param name
     * @param modTime
     */
    protected DirectoryObject(int id, int parentDirID, String path, String name, long modTime) {
        super(name, path, modTime);
        this.id = id;
        this.parentDirID = parentDirID;         
        
        dirEntries = new ArrayList();
        dbEntryExists = true;
    }
    
    /**
     * Recursively expands the subdirectories of this directory to create a file hierarchy
     *  in a depth-first sweep. Any DirectoryEntryObjects in the list of entries will be 
     *  replaced with new DirectoryObjects.
     * 
     * @return A reference to the newly-expanded DirectoryObject
     */
    public DirectoryObject expand() {
        return this;
    }
    
    /**
     * Writes the object's data back to the database
     * @return true if the operation was successful. false, otherwise
     */
    @Override
    public boolean save() {
        Logger logger = new Logger("DirectoryObject::save");
        try {
            PreparedStatement query;
            if (!dbEntryExists) { 
                query = DatabaseConnection.Instance().createQuery(
                        "INSERT INTO directories (parent_dir_id, parent_dir, name, mod_time) VALUES (?, ?, ?, ?)"); 
                if (query != null) {
                    query.setLong     (1, this.getParentDirID());
                    query.setString   (2, getParentPath());
                    query.setString   (3, getName());
                    query.setTimestamp(4, new Timestamp(this.modTime));
                    query.executeUpdate();

                    ArrayList<Long> keys = DatabaseConnection.Instance().getGeneratedKeys(query);
                    this.id = keys.get(0);

                    dbEntryExists = true;
                }         
                
                // If this is the root of the file hierarchy being scanned (its parent
                //  ID is 0), create an entry in the directory_root table
                if (parentDirID == 0) {                                                     
                    // Add entry into directory_roots table so we can track the PID of
                    //  the updater process.
                    DirectoryRoot dirRoot = new DirectoryRoot(id, this.getPath());
                    dirRoot.startTracking();
                    dirRoot.save();                  
                }
            }
            else {
                query = DatabaseConnection.Instance().createQuery(
                        "UPDATE directories SET name=?, parent_dir=?, mod_time=? WHERE d_id=?");
                if (query != null) {
                    query.setString     (1, this.name);
                    query.setString     (2, this.path);
                    query.setTimestamp  (3, new Timestamp(this.modTime));
                    query.setLong       (4, this.id); 
                    query.executeUpdate();
                }
            }

            for (FilesystemObject entry : dirEntries) {
                entry.setParentDirID(id);
                entry.save();
            }

            saveTags();

            return true;
        }
        catch (SQLException e) {
            logger.log(Logger.ERROR, String.format("Failed to save directory entry %s: %s", getPath(), e.toString()));
            return false;
        }    
    }
    
    /**
     * Deletes the object from the database
     * @return true if the operation was successful. false, otherwise
     */
    @Override
    public boolean delete() {
        try {
            PreparedStatement query = DatabaseConnection.Instance().createQuery("DELETE FROM directories WHERE d_id=?");
            query.setLong(1, id);
            query.execute();

            // Delete all entries in this directory
            for (FilesystemObject dirEntry : dirEntries) {
               dirEntry.delete();
            }
            dirEntries.clear();
            
            // If this was the root of the file hierarchy being scanned (its parent
            //  ID is 0), delete its entry from the directory_root table
            if (parentDirID == 0) {
                query = DatabaseConnection.Instance().createQuery("DELETE FROM directory_roots WHERE d_id = ?");
                query.setLong(1, id);
                query.execute();
            }
        }
        catch (SQLException e) {
            Logger logger = new Logger("DirectoryObject::delete");
            logger.log(Logger.ERROR, String.format("Failed to delete directory %s: %s", getPath(), e.toString()));
            return false;
        }
        
        return true;
    }
    
    /**
     * Reads information from the filesystem entry for this file and updates the
     *  information in the database record
     * @return 
     */
    @Override
    public boolean update() {
        File dir = new File(getPath());      
        this.modTime = dir.lastModified();
        
        String[] dirEntryNames = dir.list();
       
        // Create database entries for new filesystem entries
        for (int i = 0; i < dirEntryNames.length; i++) {
            String dirEntryName = dirEntryNames[i];
            boolean entryExistsInDb = false;
            for (FilesystemObject dirEntry : dirEntries) {
                if (dirEntry.getName().equals(dirEntryName))
                    entryExistsInDb = true;
            }
            
            if (!entryExistsInDb) {
                addNewFilesystemEntry(dirEntryName);
            }          
        }
        
        // Delete database entries which no longer have corresponding filesystem entries
        ArrayList<Integer> dirEntriesToDelete = new ArrayList();
        for (int i = 0; i < dirEntries.size(); i++) {
            FilesystemObject dirEntry = dirEntries.get(i);
            boolean existsOnFilesystem = false;
            for (String dirEntryName : dirEntryNames) {
                if (dirEntry.getName().equals(dirEntryName))
                    existsOnFilesystem = true;
            }
            
            if (!existsOnFilesystem) {
                dirEntriesToDelete.add(i);              
            }
        }
        for (int entryToDelete : dirEntriesToDelete) {
            System.out.println(path + "\\" + dirEntries.get(entryToDelete).name + " is no longer on filesystem. Deleting");
            dirEntries.get(entryToDelete).delete();
            dirEntries.remove(entryToDelete);
        }
                
        // Update all directory entries      
        for (FilesystemObject dirEntry : dirEntries) {
            dirEntry.update();
        }
        
        return true;
    }
    
    /**
     * Given the name of an entry in this directory, create a new database entry
     *  for it and add it to the list of directory entries.
     * @param dirEntryName 
     */
    private void addNewFilesystemEntry(String dirEntryName) {
        String path = Utility.concatFilePath(getPath(), dirEntryName);
        File dirEntry = new File(path);
        if (!Utility.ignoreFile(dirEntryName)) {
            if (dirEntry.isDirectory()) {
                // Check if directory already exists in database
                DirectoryEntryObject dirEntryObj = DirectoryHelper.getDirectoryByFullPath(dirEntry.getPath());
                if (dirEntryObj != null) {
                    dirEntryObj.update();
                    dirEntries.add(dirEntryObj);
                }
                else {
                    System.out.println("Creating new directory: " + dirEntry.getName());
                    DirectoryObject newDir = new DirectoryObject(dirEntry);
                    newDir.setParentDirID(id);
                    dirEntries.add(newDir);
                }
            }
            else {
                // Check if file already exists in database
                FileObject newFile = FileHelper.getFileByFullPath(dirEntry.getPath());
                if (newFile != null) {
                    newFile.update();
                    dirEntries.add(newFile);
                }
                else {           
                    newFile = new FileObject(dirEntry);
                    newFile.setParentDirID(id);
                    dirEntries.add(newFile);
                }
            }
        }
    }
    
    /**
     * Returns list of all entries, files and directories, in this directory
     * @return 
     */
    public ArrayList<FilesystemObject> getDirEntries() {
        return dirEntries;
    }
    
    public ArrayList<FileObject> getFiles() {
        ArrayList<FileObject> files = new ArrayList();
        for (FilesystemObject entry : dirEntries) {
            if (new File(entry.getPath()).isFile()) {
                files.add((FileObject) entry);
            }
        }
        return files;
    }
    
    public ArrayList<DirectoryEntryObject> getDirectories() {
        ArrayList<DirectoryEntryObject> dirs = new ArrayList();
        for (FilesystemObject entry : dirEntries) {
            if (new File(entry.getPath()).isDirectory()) {
                dirs.add((DirectoryEntryObject) entry);
            }
        }
        return dirs;
    }
    
    /**
     * Recursively adds tagToAdd to all subdirectories of this one
     * @param tagToAdd 
     */
    public void addTagRecursive(String tagToAdd) {
        addTag(tagToAdd);
        
        for (FileObject file : getFiles()) {
            file.addTag(tagToAdd);
        }
        
        ArrayList<DirectoryEntryObject> dirs = getDirectories();
        for (DirectoryEntryObject dir : dirs) {
            dir.addTagRecursive(tagToAdd);
        }
    }
    
       
    @Override
    protected String getDbName() { return DirectoryHelper.DB_NAME; }
    
    @Override
    protected String getIDFieldName() { return DirectoryHelper.ID_FIELD_NAME; }
    
    @Override
    protected String getTagTableName() { return DirectoryHelper.TAG_TABLE_NAME; }
    
    @Override
    public String toString() {
        String printStr = "Directory listing for " + getPath() + "   " + generateTagString() + "\n";
        for (FilesystemObject dirEntry : dirEntries) {
            printStr += "\t" + dirEntry.toString() + "\n";
        }
        printStr += String.format("%d file(s) \n", getFiles().size());
        printStr += String.format("%d directory(s) \n", getDirectories().size());
        
        return printStr;
    }
}
