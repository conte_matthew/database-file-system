package databasefilesystem.filesystem;

import databasefilesystem.db.DatabaseConnection;
import databasefilesystem.filesystem.DirectoryObject.DirectoryHelper;
import databasefilesystem.filesystem.FileObject.FileHelper;
import databasefilesystem.utility.Logger;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 *
 * @author Matthew
 */
public class DynamicFolder {
    
    public static class DynamicFolderHelper {
        
        public static ArrayList<String> getAllDynamicFolderNames() {
            ArrayList<String> dynamicFolderNames = new ArrayList();
            
            try {
                PreparedStatement query = DatabaseConnection.Instance().createQuery("SELECT name FROM dynamic_folders");
                ResultSet results = query.executeQuery();
                while (results.next()){
                    dynamicFolderNames.add(results.getString("name"));
                }
            }
            catch  (SQLException e) {
                Logger logger = new Logger("DynamicFolderHelper::getAllDynamicFolderNames");
                logger.log(Logger.ERROR, "Failed to retrieve dynamic folders from database: " + e);
            }
            
            return dynamicFolderNames;
        }
        
        /**
         * Returns a list of all dynamic folder objects
         * @return 
         */
        public static ArrayList<DynamicFolder> all() {
            ArrayList<DynamicFolder> dynamicFolders = new ArrayList();
            
            try {
                PreparedStatement query = DatabaseConnection.Instance().createQuery("SELECT * FROM dynamic_folders");
                ResultSet results = query.executeQuery();
                while (results.next()){
                    dynamicFolders.add(new DynamicFolder(results.getLong("folder_id"),
                                                         results.getString("name"),
                                                         results.getString("description"),
                                                         results.getInt("hasFiles"),
                                                         results.getInt("hasDirectories")));
                }
            }
            catch  (SQLException e) {
                Logger logger = new Logger("DynamicFolderHelper::all");
                logger.log(Logger.ERROR, "Failed to retrieve dynamic folders from database: " + e);
            }
            
            return dynamicFolders;
        }
        
        /**
         * Replaces spaces with '_' in the user's selected folder name so that the
         *  name will be accepted as a view name in the database
         * @param name
         * @return 
         */
       private static String sanitizeFolderName(String name) {
           return name.replace(" ", "_");
       }
    }
    
    private long    id;
    private String  name;
    private String  description;
    private int     hasFiles;
    private int     hasDirectories;
    private String  fileQuery;
    private String  directoryQuery;
    
    private ArrayList<? extends FilesystemObject> folderContents;
    
    private boolean dbEntryExists;
    
    public DynamicFolder(long id, String name, String description, int hasFiles, int hasDirectories) {
        this.id             = id;
        this.name           = name;
        this.description    = description;
        this.hasFiles       = hasFiles;
        this.hasDirectories = hasDirectories;
        
        folderContents = new ArrayList();
        
        this.dbEntryExists  = true;
    }
    
    public DynamicFolder(String name, String description, String fileQuery, String directoryQuery) {
        this.name            = name;
        this.description     = description;
        this.fileQuery       = fileQuery;
        this.directoryQuery  = directoryQuery;
        
        if (!fileQuery.equals(""))
            this.hasFiles = 1;
        if (!directoryQuery.equals(""))
            this.hasDirectories = 1;
        
        folderContents = new ArrayList();
        
        this.dbEntryExists = false;
    }
    
    /**
     * Populate the folterContents list with the files and/or directories which are
     *  returned for this folder's query(ies)
     */
    public DynamicFolder expand() {
        folderContents.clear();
        ArrayList files = new ArrayList();
        ArrayList directories = new ArrayList();
        try {
            if (hasFiles == 1) {
                String fileViewName = DynamicFolderHelper.sanitizeFolderName(name) + "_files";
                PreparedStatement fileQuery = DatabaseConnection.Instance().createQuery("SELECT * FROM " + fileViewName);
                files = FileHelper.createObjectsFromResultSet(fileQuery.executeQuery());
            }
            if (hasDirectories == 1) {
                String dirViewName = DynamicFolderHelper.sanitizeFolderName(name) + "_directories";
                PreparedStatement dirQuery = DatabaseConnection.Instance().createQuery("SELECT * FROM " + dirViewName);
                directories = DirectoryHelper.createObjectsFromResultSet(dirQuery.executeQuery());
            }
            
            folderContents.addAll(files);
            folderContents.addAll(directories);
        }
        catch (SQLException e) {
            Logger logger = new Logger("DynamicFolder::expand");
            logger.log(Logger.ERROR, "Failed to retrieve contents of dynamic folder " + name + " from database: " + e);
        }
        
        return this;
    }
    
    public boolean save() {
        if (!dbEntryExists) {
            try {
                String queryStr = "INSERT INTO dynamic_folders (name, description, hasFiles, hasDirectories) VALUES (?, ?, ?, ?)";
                PreparedStatement query = DatabaseConnection.Instance().createQuery(queryStr);
                query.setString(1, name);
                query.setString(2, description);
                query.setInt   (3, hasFiles);
                query.setInt   (4, hasDirectories);
                query.execute();

                if (!fileQuery.equals("")) {
                    queryStr = "CREATE VIEW " + DynamicFolderHelper.sanitizeFolderName(name) + "_files AS " + fileQuery;
                    query = DatabaseConnection.Instance().createQuery(queryStr);
                    query.execute();
                }

                if (!directoryQuery.equals("")) {
                    queryStr = "CREATE VIEW " + DynamicFolderHelper.sanitizeFolderName(name) + "_directories AS " + directoryQuery;
                    query = DatabaseConnection.Instance().createQuery(queryStr);
                    query.execute();
                }
            }
            catch (SQLException e) {
                Logger logger = new Logger("DynamicFolder::save");
                logger.log(Logger.ERROR, "Failed to create dynamic folder " + name + ": " + e);
                return false;
            }
            
            dbEntryExists = true;
        }
        else {
            
        }
        
        return true;
    }
    
    /**
     * Deletes the entry for this folder from the database and removes the associated
     *  file and/or directory views
     * @return 
     */
    public boolean delete() {
        if (dbEntryExists) {
            try {
                PreparedStatement query = DatabaseConnection.Instance().createQuery("DELETE FROM dynamic_folders WHERE folder_id=?");
                query.setLong(1, id);
                query.execute();
                
                if (hasFiles == 1) {
                    query = DatabaseConnection.Instance().createQuery("DROP VIEW " + DynamicFolderHelper.sanitizeFolderName(name) + "_files");
                    query.execute();
                }
                if (hasDirectories == 1) {
                    query = DatabaseConnection.Instance().createQuery("DROP VIEW " + DynamicFolderHelper.sanitizeFolderName(name) + "_directories");
                    query.execute();
                }
            }
            catch (SQLException e) {
                Logger logger = new Logger("DynamicFolder::delete");
                logger.log(Logger.ERROR, "Failed to delete dynamic folder " + name + ": " + e);
                return false;
            }
            
            dbEntryExists = false;
            
        }
        return true;
    }
    
    public String getName() { return name; }
    
    public String toString() {
        String folderStr = name + "\n" + description + "\n";
        for (FilesystemObject obj : folderContents) {
            folderStr += "\t" + obj.toString() + "\n";
        }       
        
        return folderStr;
    }
}
