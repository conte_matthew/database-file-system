
package databasefilesystem.filesystem;

import databasefilesystem.db.DatabaseConnection;
import databasefilesystem.utility.Logger;
import java.io.File;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 * Proxy class for DirectoryObject. Contains an unexpanded directory object
 *  (tracks the directory's metadata but does not scan it for its file or directory
 *  entries.
 * @author Matthew
 */
public class DirectoryEntryObject extends DirectoryObject {
    private DirectoryObject dirObj;
    
    /**
     * External constructor for creating a new directory entry from the information
     *  found on the filesystem
     * @param dir 
     */
    public DirectoryEntryObject(File dir) {
        super(dir);
        dirObj = null;
    }
    
    /**
     * Internal constructor for building an object form information in the database
     * @param id
     * @param parentDirID
     * @param path
     * @param name
     * @param modTime 
     */
    protected DirectoryEntryObject(int id, int parentDirID, String path, String name, long modTime) {
        super(id, parentDirID, path, name, modTime);     
        dbEntryExists = true;
    }
    
    /**
     * Scan any subdirectories, recursively making entries for the discovered
     *  files and directories
     * @return 
     */
    @Override
    public DirectoryObject expand() {       
        if (dirObj == null) {
            this.dirObj = new DirectoryObject(id);
        }
        return this.dirObj;
    }
    
    /**
     * Expands the directory entry, retrieving entries for the files and directories
     *  it contains and returns a list of the underlying (unexpanded) DirectoryEntryObjects
     *  and FileObjects
     * @return 
     */
    @Override
    public ArrayList<FilesystemObject> getDirEntries() {
        expand();
        return dirObj.getDirEntries();
    }
    
    /**
     * Expands the directory entry, retrieving entries for the files and directories
     *  it contains and returns a list of the underlying FileObjects
     * @return 
     */
    @Override
    public ArrayList<FileObject> getFiles() {
        expand();
        return dirObj.getFiles();
    }
    
    /**
     * Expands the directory entry, retrieving entries for the files and directories
     *  it contains and returns a list of the underlying (unexpanded) DirectoryEntryObjects
     * @return 
     */
    @Override
    public ArrayList<DirectoryEntryObject> getDirectories() {
        expand();
        return dirObj.getDirectories();
    }
    
    @Override
    public boolean save() {
        if (dirObj != null)
            return dirObj.save();
        
        return super.save();
    }
    
    /**
     * Deletes the object from the database
     * @return true if the operation was successful. false, otherwise
     */
    @Override
    public boolean delete() {
        if (dirObj != null)
            return dirObj.delete();
        
        return expand().delete();
    }
    
    @Override
    public boolean update() {
        expand();
        return dirObj.update();
    }
    
    @Override
    public void setParentDirID(long parentDirID) { 
        this.parentDirID = parentDirID; 
        if (dirObj != null)
            dirObj.setParentDirID(parentDirID);
    }
    
    @Override
    public void addTagRecursive(String tagToAdd) {
        expand();
        super.addTagRecursive(tagToAdd);
    }
    
    @Override
    public ArrayList<Tag> getTags() { 
        if (dirObj != null)
            return dirObj.getTags();
        
        return super.getTags(); 
    }
    
    @Override
    public String toString() {
        String dirEntryStr = String.format("%-20s %-5s  %-10s %-50s",
                              new SimpleDateFormat("MM/dd/yyyy KK:mm a").format(getLastModTime()),
                              "<DIR>",
                              "",
                              getName());
        
        dirEntryStr += generateTagString();
        
        return dirEntryStr;
    }

    
}
