
package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.DirectoryObject;
import databasefilesystem.filesystem.DirectoryRoot;
import databasefilesystem.filesystem.DirectoryRoot.DirectoryRootHelper;
import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Matthew
 */
public class ManageDirectories implements Command {
    
    public ManageDirectories() {    }
    
    
    /**
     * Display list of directory roots currently being tracked by DFS. Prompt user to select one whose
     *  contents to display or to delete.
     */
    @Override
    public boolean execute() {
        ArrayList<DirectoryRoot> directoryRoots = DirectoryRootHelper.all();   
        if (directoryRoots.isEmpty()) {
            System.out.println("DFS is not currently tracking any directories");
            return true;
        }

        Scanner input = new Scanner(System.in);
        int dirNum = 0;
        while (true) {
            int i = 1;
            System.out.println("\nDirectory roots being tracked:");
            for (DirectoryRoot root : directoryRoots) {
                System.out.println("\t" + i + ". " + root);
                i++;
            }

            System.out.print("\nEnter 'display', 'delete', 'startTracking', or 'stopTracking'"
                            +  " followed by the directory number (Press 'Enter' to return to main menu): ");
            String line = input.nextLine();
            if (line.equals(""))
                break;
            
            String[] cmdStr = line.split(" ");
            String cmd = cmdStr[0];
            dirNum = Integer.parseInt(cmdStr[1])-1;
            
            switch (cmd) {
                case "display":
                    System.out.println(new DirectoryObject(directoryRoots.get(dirNum).getDirID()));
                    break;
                case "delete":
                    directoryRoots.get(dirNum).delete();
                    directoryRoots.remove(dirNum);
                    break;
                case "startTracking":
                    directoryRoots.get(dirNum).startTracking();
                    break;
                case "stopTracking":
                    directoryRoots.get(dirNum).stopTracking();
                    break;
            }
            
        }
        
        return true;
    }
    
    @Override
    public DFSResultSet getResultSet() {
        return new DFSResultSet();
    }
    
    @Override
    public boolean hasResults() { return false; }
    
    @Override
    public boolean updatesWorkingSet() { return false; }
    
}
