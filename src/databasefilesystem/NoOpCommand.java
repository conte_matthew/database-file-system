package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;

public class NoOpCommand implements Command {
    
    @Override
    public boolean execute () { return true; }   
    
    @Override
    public DFSResultSet getResultSet() {
        return new DFSResultSet();
    }
    
    @Override
    public boolean hasResults() { return false; }
    
    @Override
    public boolean updatesWorkingSet() { return false; }
    
}
