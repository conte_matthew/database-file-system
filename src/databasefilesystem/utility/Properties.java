
package databasefilesystem.utility;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.ArrayList;

/**
 *  Singleton which reads in the properties.txt file and provides these configuration
 *      parameters to the rest of the DFS
 * @author Matthew
 */
public class Properties {
    public class Property implements Comparable<Property> {
        private String name;
        private String value;
        
        public Property(String name, String value) {
            this.name = name;
            this.value = value;
        }
        
        public Property(String name) {
            this.name = name;
            this.value = "";
        }
        
        @Override
        public int compareTo(Property other) {
            return name.compareTo(other.name);
        }
        
        public String getValue() { return value; }
    }
    
    private static Properties propertiesObj = null;
    
    private ArrayList<Property> properties;
    
    public static Properties Instance() {
        if (propertiesObj == null)
            propertiesObj = new Properties();
        
        return propertiesObj;    
    }
    
    /**
     * Reads in the properties.txt file and parses it to extract the individual
     *  parameters.
     */
    protected Properties() {
        properties = new ArrayList();
        
        String path = System.getProperty("user.dir") + "\\properties\\properties.txt";
        File propFile = new File(path);
        try {
            BufferedReader in = new BufferedReader(new FileReader(propFile));
            String line;
            while ((line = in.readLine()) != null) {
                if (!line.isEmpty()) {
                    String[] lineSplit = line.split(" ");
                    properties.add(new Property(lineSplit[0], lineSplit[1]));
                }
            }
        }
        catch (Exception e) {
            Logger logger = new Logger("Properties::Properties");
            logger.log(Logger.ERROR, "Failed to read in properties file: " + e);
        }
    }
    
    public String getProperty(String propertyName) {
        for (Property p : properties) {
            if (p.name.equals(propertyName))
                return p.value;
        }  
        
        Logger logger = new Logger("Properties::lookupPropertyValue");
        logger.log(Logger.ERROR, "Property " + propertyName + " does not exist.");
        return "";
    }
}
