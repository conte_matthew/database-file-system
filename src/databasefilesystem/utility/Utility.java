
package databasefilesystem.utility;

import databasefilesystem.db.DatabaseFilterParam;
import databasefilesystem.filesystem.FilesystemObject;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 * General helper functions
 * @author Matthew
 */
public class Utility {
    
    public static String concatFilePath(String path, String name) {
        return path + "\\" + name;
    }
    
    public static String concatFileExtension(String name, String ext) {
        return name + "." + ext;
    }
    
    /** Given a list of DatabaseFilterParams, returns the resulting WHERE clause.
     * For example, for the following filterParams:
     *     <"x", "=", 4>
     *     <"y", ">", 21>
     *     <"z", "!=", "Hello">
     * the resulting WHERE clause would be:
     *      "x = 4 AND y > 21 AND z != 'Hello'"
     * 
     * *NOTE* The returned string does not include 'WHERE' at the beginning. This
     *  allows the caller to chain this clause with any other WHERE requirements
     *  (such as equality checks for IDs when joining tables) using an 'AND'
     * @return String containing the resulting WHERE clause
     */
    public static String buildWhereClause(ArrayList<DatabaseFilterParam> filterParams) {
        String where = "";
        
        int paramCount = 0;
        for (DatabaseFilterParam param : filterParams) {
            if (paramCount > 0)
                where += " AND ";
            
            where += param.fieldName   + " " + 
                     param.comparator  + " " +
                     "'" + param.value.replace("\\", "\\\\") + "'";
            
            paramCount++;           
        }
        
        return where;
    }
    
    /**
     * The ResultSet object does not have a function which gives the number of
     *  rows that were returned from the database. This function counts the rows
     *  by looping through the ResultSet object. It then resets the ResultSet to
     *  before the first entry
     * 
     * @param results
     * @return 
     */
    public static int getRowsInResultSet(ResultSet results) {
        int rowCount = 0;
        try {
            if (results.last()) {
                rowCount = results.getRow();
            }
            results.beforeFirst();
        }
        catch (SQLException e) {
            Logger logger = new Logger("DatabaseObject::getRowsInResultSet");
            logger.log(Logger.ERROR, "Failed to retrieve row count from ResultSet: " + e);
        }       
        
        return rowCount;
    }
    
    /**
     * Returns true if str is one of the comparators supported by the DFS ('=', 
     *  '>', ">=', '<', '<=', 'LIKE') and false otherwise.
     * @param str
     * @return 
     */
    public static boolean isComparator(String str) {
        if (str.equals("=") || str.equals(">")  || str.equals(">=") || 
            str.equals("<") || str.equals("<=") || str.equals("LIKE"))
            return true;
        
        return false;
    }
    
    /**
     * Given a list of filesystem objects, sorts the list by parent directory
     * @param list 
     */
    public static void sortListByDirectory(ArrayList<? extends FilesystemObject> list) {
        Collections.sort(list, new Comparator<FilesystemObject>() {
            @Override
            public int compare(FilesystemObject fsObj1, FilesystemObject fsObj2)
            {
                return  fsObj1.getPath().compareTo(fsObj2.getPath());
            }
        });
    }
    
}
