package databasefilesystem.utility;

/**
 *
 * @author Matthew
 */
public class Logger {   
    public static final int ERROR   = 0; 
    public static final int INFO    = 1;
    public static final int DEBUG   = 2;

    // Move to a properties file later
    private static final int LOG_LEVEL = DEBUG;
    
    private String location;
    
    public Logger(String location)
    {
        this.location = location;
    }
    
    public void log (int logLevel, String msg) 
    {
        if (logLevel <= LOG_LEVEL) {
            if (logLevel == ERROR)
                System.err.println("ERROR in: " + location + ": " + msg);
            else
                System.out.println(location + ": " + msg);
        }
    }
    
}
