
package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;
import databasefilesystem.filesystem.FilesystemObject;
import databasefilesystem.utility.Logger;
import databasefilesystem.utility.Properties;
import databasefilesystem.utility.Utility;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

/**
 *
 * @author Matthew
 */
public class DatabaseFileSystem {
    // Command names available via the CLI
    private static final String SCAN_CMD                = "scan";
    private static final String SEARCH_CMD              = "search";
    private static final String SHOW_ALL                = "show_all";
    private static final String ADD_TAG                 = "addTag";
    private static final String REMOVE_TAG              = "removeTag";
    private static final String MANAGE_DYNAMIC_FOLDERS  = "dynamicFolders";
    private static final String SAVE_DYNAMIC_FOLDER     = "saveDynamicFolder";
    private static final String MANAGE_DIRECTORIES      = "manageDirectories";
    private static final String EXIT                    = "exit";

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        Properties properties = Properties.Instance();        
        
        DFSResultSet workingSet = null;
        DFSResultSet resultSet  = null;
        
        boolean cmdPassedIn = false;  // Set to true if a command was passed in from command line
        
        System.out.println("Welcome to the Database File System");
        
        boolean displayWorkingSet = false;
        while (true) {           
            if (!(workingSet == null) && displayWorkingSet) {
                System.out.println("\nActive Working Set: ");       
                displayResultSetByDirectory(workingSet.getResults());
            }
            
            displayWorkingSet = true;
            
            System.out.println();
            
            List<String> cmdList;
            String cmdStr = "";
            if (args.length == 0) {
                Scanner input = new Scanner(System.in);
                System.out.print("Command: ");
                cmdStr = input.nextLine();
                cmdList = Arrays.asList(cmdStr.split(" "));
            }
            else {
                cmdPassedIn = true;
                cmdList = Arrays.asList(args);
                for (String arg : cmdList)
                    cmdStr += arg + " ";
            }

            if (cmdList.isEmpty()) {
                System.out.println("No command passed in. Nothing to do...");
                System.exit(1);
            }
            int resultSetNum;
            Command cmd = new NoOpCommand();
            switch (cmdList.get(0)) {
                case SCAN_CMD:
                    cmd = new ScanFileHierarchyCommand(cmdList.get(1));
                    break;
                case SEARCH_CMD:
                    cmd = new SearchCommand(cmdStr);
                    break;
                case SHOW_ALL:
                    cmd = new ShowAllCommand(cmdStr);
                    break;
                case ADD_TAG:                  
                    try {
                        resultSetNum = Integer.parseInt(cmdList.get(1));
                        if (resultSetNum < 1 || resultSetNum > workingSet.getResults().size()) 
                            System.err.println("Invalid selection. Must choose object in the current working set");
                        else
                            cmd = new AddTagCommand(workingSet.getResults().get(resultSetNum-1), cmdList.get(2));
                    }
                    catch (NumberFormatException e) {
                        System.err.println(e);
                    }
                    break;
                case REMOVE_TAG:
                    try {
                        resultSetNum = Integer.parseInt(cmdList.get(1));
                        if (resultSetNum < 1 || resultSetNum > workingSet.getResults().size()) 
                            System.err.println("Invalid selection. Must choose object in the current working set");
                        else
                            cmd = new RemoveTagCommand(workingSet.getResults().get(resultSetNum-1), cmdList.get(2));
                    }
                    catch (NumberFormatException e) {
                        System.err.println(e);
                    }
                    break;
                case MANAGE_DYNAMIC_FOLDERS:
                    cmd = new ManageDynamicFolders();
                    break;
                case SAVE_DYNAMIC_FOLDER:
                    if (workingSet == null) {
                        System.err.println("Working set is empty. Run search query first in order to save it as a dynamic folder");
                    }
                    else {
                        cmd = new CreateDynamicFolder(workingSet);
                    }
                    break;
                case MANAGE_DIRECTORIES:
                    cmd = new ManageDirectories();
                    break;
                case EXIT:
                    System.exit(0);
                default:
                    System.err.println("Invalid command: " + cmdList.get(0));
            }

            if (cmd.execute()) {
                if (cmd.updatesWorkingSet()) {
                    workingSet = cmd.getResultSet();
                    Utility.sortListByDirectory(workingSet.getResults());
                }
                
                if (cmd.hasResults()) {
                    System.out.println("\nResults:");
                    resultSet = cmd.getResultSet();
                    Utility.sortListByDirectory(resultSet.getResults());
                    displayResultSetByDirectory(resultSet.getResults());
                    displayWorkingSet = false;
                }
            }     
            
            if (cmdPassedIn)
                System.exit(0);
        }    
    }
    
    /**
     * Displays a list of filesystem objects ordered by parent directory.
     * @param list 
     */
    public static void displayResultSetByDirectory(ArrayList<? extends FilesystemObject> workingSet) {
        String parentDir = "";
        for (int i = 0; i < workingSet.size(); i++) {
            FilesystemObject obj = workingSet.get(i);
            if (!obj.getParentPath().equals(parentDir)) {
                parentDir = obj.getParentPath();
                System.out.println(parentDir);
            }
            
            System.out.println(String.format("    %d. %s", (i+1), obj));           
        }
        
    }
    
}
