package databasefilesystem;

import databasefilesystem.filesystem.DFSResultSet;

/**
 * Interface which is implemented by all available DFS commands
 * @author Matthew
 */
public interface Command {
    public boolean execute();
    public DFSResultSet getResultSet();
    public boolean hasResults();
    public boolean updatesWorkingSet();
}
